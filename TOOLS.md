Useful tool commands

When needing to re-compile a file from dsrc, these commands can be helpfull when only needing to compile 1 or a few files.

	1. DataTableTool (for .tab files) syntax is: tools/DataTableTool -i $file -o $outputfile ifile should equal the full path to the file you are compiling, output file should equal the full output path (same as the first but replace dsrc with data and .tab with .iff)
	2. Miff (for .mif files) syntax is the same as DataTableTool
	3. Java files (for scripts) you will want to run this from the dsrc/sku.0/sys.server/compiled/game/ directory. Example: javac -classpath c:/users/swg/swg-src/data/sku.0/sys.server/compiled/game/ -d c:/users/swg/swg-src/data/sku.0/sys.server/compiled/game/ -g deprecation script/space/battlefields/battlefield_manager.java
	4. TemplateCompiler (for .tpf files) tools/templateCompiler -compile "full path to file" The template compiler will automagically create the file in the correct location under the "data" dir